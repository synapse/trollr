use chumsky::prelude::*;

use crate::ast;

pub fn expr(arena: &bumpalo::Bump) -> impl Parser<char, &mut ast::Expr, Error = Simple<char>> {
    recursive(|sub_expr| {
        // FIXME: Replace `.unwrap()` with graceful Ariadne failure
        let num =
            text::digits(10).map(|s: String| arena.alloc(ast::Expr::N(s.parse::<u64>().unwrap())));

        // (<expr>)
        let parens = sub_expr.clone().delimited_by(just('('), just(')'));

        // ---

        // d<expr>
        let die = just('d')
            .ignore_then(sub_expr.clone())
            .map(|n: &mut ast::Expr| arena.alloc(ast::Expr::D(n)));

        // <num>d<expr>
        let n_die = num
            .then(die.clone().or_not())
            .map(|(n, die_opt)| match die_opt {
                Some(die) => arena.alloc(ast::Expr::Arr(n, die)),
                None => n,
            });

        // (<expr>)d<expr>
        let parens_die = parens
            .then(die.clone().or_not())
            .map(|(n, die_opt)| match die_opt {
                Some(die) => arena.alloc(ast::Expr::Arr(n, die)),
                None => n,
            });

        choice((die, n_die, parens_die))
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn dice_1d6() {
        let arena = bumpalo::Bump::new();
        let parse = expr(&arena).parse("d6");
        match parse {
            Ok(result) => println!("{:#?}", result),
            Err(why) => println!("{:#?}", why),
        }
    }
}
