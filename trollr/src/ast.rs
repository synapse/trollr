#[derive(Debug)]
pub enum Expr<'a> {
    N(u64),
    D(&'a Expr<'a>),
    Arr(&'a Expr<'a>, &'a Expr<'a>),
}
