use ariadne::{Color, Label, Report, ReportKind, Source};
use chumsky::Parser as _;
use clap::{command, Parser as _};

fn main() {
    let args = Cli::parse();
    let arena = bumpalo::Bump::new();
    let parse = trollr::expr(&arena).parse(args.expr.clone());
    match parse {
        Ok(result) => println!("{:#?}", result),
        Err(why) => {
            for err in why.iter() {
                let message = format!("{:?}", err.reason());
                Report::build(ReportKind::Error, (), err.span().start)
                    .with_message(err.to_string())
                    .with_label(
                        Label::new(err.span())
                            .with_message(message)
                            .with_color(Color::Red),
                    )
                    .finish()
                    .print(Source::from(&args.expr))
                    .unwrap()
            }
        }
    }
}

#[derive(Debug, clap::Parser)]
#[command(version, about, long_about = None)]
pub struct Cli {
    /// A dice roll expression, e.g. '3d6'
    expr: String,
}
